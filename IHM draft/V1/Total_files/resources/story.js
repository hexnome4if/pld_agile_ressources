var story = {
 "pages": [
  {
   "image": "AjouterLivraison.png",
   "image2x": "AjouterLivraison@2x.png",
   "width": 617,
   "links": [],
   "title": "AjouterLivraison",
   "height": 241
  },
  {
   "image": "FenPrincipale.png",
   "image2x": "FenPrincipale@2x.png",
   "width": 617,
   "links": [
    {
     "rect": [
      10,
      79,
      144,
      103
     ],
     "page": 0
    },
    {
     "rect": [
      175,
      384,
      309,
      408
     ],
     "page": 3
    }
   ],
   "title": "FenPrincipale",
   "height": 425
  },
  {
   "image": "InitProgramme.png",
   "image2x": "InitProgramme@2x.png",
   "width": 329,
   "links": [{
    "rect": [
     99,
     192,
     242,
     216
    ],
    "page": 1
   }],
   "title": "InitProgramme",
   "height": 225
  },
  {
   "image": "ModifierLivraison.png",
   "image2x": "ModifierLivraison@2x.png",
   "width": 617,
   "links": [],
   "title": "ModifierLivraison",
   "height": 241
  }
 ],
 "resolutions": [2],
 "title": "Total",
 "highlightLinks": true
}