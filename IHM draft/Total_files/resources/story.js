var story = {
 "pages": [
  {
   "image": "AjouterLivraison.png",
   "image2x": "AjouterLivraison@2x.png",
   "width": 617,
   "links": [
    {
     "rect": [
      409,
      184,
      563,
      208
     ],
     "page": 1
    },
    {
     "rect": [
      64,
      184,
      221,
      208
     ],
     "page": 1
    }
   ],
   "title": "AjouterLivraison",
   "height": 241
  },
  {
   "image": "FenPrincipale.png",
   "image2x": "FenPrincipale@2x.png",
   "width": 769,
   "links": [
    {
     "rect": [
      34,
      79,
      168,
      103
     ],
     "page": 0
    },
    {
     "rect": [
      10,
      383,
      168,
      407
     ],
     "page": 3
    }
   ],
   "title": "FenPrincipale",
   "height": 505
  },
  {
   "image": "InitProgramme.png",
   "image2x": "InitProgramme@2x.png",
   "width": 329,
   "links": [{
    "rect": [
     99,
     192,
     242,
     216
    ],
    "page": 1
   }],
   "title": "InitProgramme",
   "height": 225
  },
  {
   "image": "ModifierLivraison.png",
   "image2x": "ModifierLivraison@2x.png",
   "width": 617,
   "links": [
    {
     "rect": [
      409,
      184,
      563,
      208
     ],
     "page": 1
    },
    {
     "rect": [
      64,
      184,
      221,
      208
     ],
     "page": 1
    }
   ],
   "title": "ModifierLivraison",
   "height": 241
  }
 ],
 "resolutions": [2],
 "title": "Total",
 "highlightLinks": true
}